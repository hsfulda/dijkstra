/*
 * Filename:    dijkstra.c          Author:     Touhidur Rahman
 * Date:        07 Feb 2017
 *
 * This program outputs the length of a shortest path from 
 * vertex 0 to every vertex. Uses Message Passing Interface (MPI)
 *
 * Compile:
 *
 * $ mpicc dijkstra.c -o dijkstra
 *
 * Run:
 *
 * $ mpiexec -np <nProcs> dijkstra <n>
 *
 *            n:  # of rows and columns in the matrix
 *
 *  Notes: The number of processes should evenly divide
 *  the number of vertices (n).
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mpi.h>

#define INFINITY 1000000
#define N_VTX 10000
//#define DEBUG 1                       /* uncomment to debug mode on   */

void readMatrix (int localMatrix[], int n, int nLocal,
                MPI_Datatype slicedMatrix, int rank, MPI_Comm comm);
int findMinDist (int localDist[], int localVisited[], int nLocal);
void dijkstra (int localMatrix[], int localDist[],
              int n, int nLocal, int rank, MPI_Comm world);
MPI_Datatype sliceMatrixByColumn (int n, int nLocal);
void printDists(int dist[]);

int main (int argc, char *argv[])
{
  int i, j,
      n,                                /* # of matrix rows or cols     */
      nLocal,                           /* # of localMatrix cols        */
      *localMatrix,                     /* sliced matrix                */
      *localDist,                       /* array of dists in local mtx  */
      *dist,                            /* array of all dists           */
      worldsize,                        /* communicator size            */
      rank;                             /* rank of process              */
  MPI_Comm world;                       /* MPI communicator             */
  double T1, T2;                        /* start and finish time        */
  /* special MPI_Datatype to slice bigger matrix into submatrices       */
  MPI_Datatype SUB_MTX;

  MPI_Init(&argc, &argv);
  world = MPI_COMM_WORLD;
  MPI_Comm_size(world, &worldsize);     /* get the number of procs      */
  MPI_Comm_rank(world, &rank);          /* get the rank of the proc     */
  n = N_VTX;                            /* number of vertices           */
  MPI_Bcast(&n, 1, MPI_INT, 0, world);  /* broadcast n to other procs   */
  nLocal = n / worldsize;

  /* allocate memory space before operation                             */
  localMatrix = malloc (n * nLocal * sizeof(int));
  localDist   = malloc (nLocal * sizeof(int));
  dist        = malloc (n * sizeof(int));

  /* get the submatrix for this process                                 */
  SUB_MTX = sliceMatrixByColumn (n, nLocal);

  /* read matrix from file & distribute sub matrix to all processes     */
  readMatrix (localMatrix, n, nLocal, SUB_MTX, rank, world);

  /* if the MPI rank is 0, then measure time at this process            */
  if (rank == 0)
  {
    T1 = MPI_Wtime();
  }

  dijkstra (localMatrix, localDist, n, nLocal, rank, world);

  /* gather local dists into one for printing on display                */
  MPI_Gather (
    localDist, nLocal, MPI_INT, dist, nLocal, MPI_INT, 0, world
    );

  T2 = MPI_Wtime ();                     /* record end time              */
  if (rank == 0)
  {
    printDists (dist);                  /* print dists                  */
    printf ("Elapsed time at node 0: %f\n", (float) (T2 - T1));
  }
  MPI_Finalize();
  return EXIT_SUCCESS;
}


/**
 * Builds a MPI_Datatype that consists a number of columns
 * of the original matrix. The # of rows remain same as original
 * @input    nCols:  # of rows or column in the matrix
 *           nColsForSlice:  number cols in the block column
 * @output   SUB_MTX: MPI_Datatype that represents a block column
 */
MPI_Datatype sliceMatrixByColumn (int nCols, int nColsForSlice)
{
  MPI_Aint                              /* mem addr, similar to   *int  */
      lb, extent;                       /* lower bound and span         */
  MPI_Datatype block;                   /* tmp, stores # col of sub mtx */
  MPI_Datatype stack;                   /* tmp, vector/stack of blocks  */
  MPI_Datatype SUB_MTX;                 /* final type                   */
  /* allows replication of a datatype into contiguous locations.        */
  MPI_Type_contiguous (nColsForSlice, MPI_INT, &block);
  /* returns the lower bound and the extent of datatype.                */
  MPI_Type_get_extent (block, &lb, &extent);
  /* replicates datatype into locations consisting equally spaced blocks*/
  MPI_Type_vector (nCols, nColsForSlice, nCols, MPI_INT, &stack);
  MPI_Type_create_resized (stack, lb, extent, &SUB_MTX);
  MPI_Type_commit (&SUB_MTX);
  MPI_Type_free (&block);
  MPI_Type_free (&stack);
  return SUB_MTX;
}


/**
 * Reads an adjacency matrix from file, stores in an array and
 * distributes sliced matrices among other processes
 * @input   n:  # of rows in the matrix and the submatrices
 *          nLocal = n/worldsize:  # of columns in the submatrices
 *          SUB_MTX:  MPI_Datatype used on process 0
 *          rank:  the caller proc's rank in world
 *          world:  MPI Communicator
 * @output  localMatrix:  the calling proc's submatrix
 * Sideeffects: this function may take same time because it reads
 * file from disk. This time is not recorded
 * as our goal is to benchmark the performance of MPI execution
 */
void readMatrix (int *localMatrix, int n, int nLocal,
           MPI_Datatype SUB_MTX, int rank, MPI_Comm world)
{
  int i, j,
      *mat;                             /* val from file to be stored   */
  FILE *fp;
  char str[60];
  if (rank == 0) {
    mat = malloc (n * n * sizeof(int));
    for ( i = 0; i < N_VTX; i++ )       /* initialize mtx with infinite */
    {
      for ( j = 0; j < N_VTX; j++ )
      {
        mat[i * N_VTX + j] = INFINITY;
      }
    }
    
    fp = fopen ("input.txt" , "r");     /* opening file for reading     */
    if(fp == NULL)
    {
      perror ("Error opening file");
    }
    while (!feof (fp))
    {
      /* read in the line and make sure it was successful               */
      if (fgets (str,60,fp) != NULL)
      {
        const char s[2] = "\t";
        char* sv = strtok (str,s);      /* source vertex                */
        char* dv = strtok (NULL,s);     /* destination vertex           */
        char* wtStr = strtok (NULL,s);
        int wt = atoi (wtStr);          /* distance of edge, sv to dv   */
        
        mat[atoi (sv) * N_VTX + atoi (dv)] = wt;
#ifdef DEBUG
        printf (" %s\t\t<--->\t\t%s\t\t= %d \n", sv, dv, wt);
#endif
      }
    }
    fclose (fp);
  }

  MPI_Scatter (
    mat,                                /* send data buffer             */
    1,                                  /* send count                   */
    SUB_MTX,                            /* send data type               */
    localMatrix,                        /* recv data buffer             */
    n * nLocal,                         /* recv count                   */
    MPI_INT,                            /* recv data type               */
    0,                                  /* root                         */
    world                               /* communicator                 */
  );

  if (rank == 0)
  {
    free (mat);
  }
}


/**
 * Finds the minimum dist within a submatrix
 * @input  localDist: array of distances in local matrix
 *         localVisited: array of visited vertices
 *         nLocal: # of columns in local submatrix
 * @output min: the vertex which holds min dist
 */
int findMinDist (int *localDist, int *localVisited, int nLocal) {
  int u,                                /* a unvisited src vtx          */
    v,                                  /* vtx, path to which to be ckd */
    current;                            /* dist found best so far       */
  u = -1;
  current = INFINITY;
  /* determine which path is better to reach v? 0..v or 0..u->v?        */
  for (v = 0; v < nLocal; v++) {
    if (!localVisited[v]) {
      if (localDist[v] < current) {
        u = v;
        current = localDist[v];
      }
    }
  }
  return u;
}


/**
 * Calculates the shortest path distance for local sub matrices and
 * gathers in root process
 * @input  localMatrix: block of matrix derived from bigger matrix
 *         localDist: array where distances are stored
 *         n: # of rows/cols of original matrix
 *         nLocal: # of cols in sub matrix
 *         rank: rank of process
 *         world: MPI Communicator
 * @output none
 */
void dijkstra (int localMatrix[], int localDist[],
               int n, int nLocal, int rank, MPI_Comm world)
{
  int localVisited[nLocal], uLocal, vLocal, i, u, uDist, newDist;
  int minDistLocal[2];
  int minDist[2];

  for (vLocal = 0; vLocal < nLocal; vLocal++)
  {
    /* as dist of 0->v = mat[0][v]                                      */
    localDist[vLocal] = localMatrix[0 * nLocal + vLocal];
    localVisited[vLocal] = 0;           /* set as not visited yet       */
  }

  if (rank == 0)
  {
    localVisited[0] = 1;                /* 0-th src considered visited  */
  }

  for (i = 1; i < n; i++)
  {
    uLocal = findMinDist (localDist, localVisited, nLocal);
    if (uLocal >= 0)
    {
      minDistLocal[0] = localDist[uLocal];      /* distance             */
      minDistLocal[1] = uLocal + rank * nLocal; /* which vertex         */
    }
    else
    {
      minDistLocal[0] = INFINITY;
      minDistLocal[1] = -1;
    }

    MPI_Allreduce (
      minDistLocal,                     /* send data buffer             */
      minDist,                          /* recv data buffer             */
      1,                                /* recv data count              */
      MPI_2INT,                         /* 2INT gives val and loc       */
      MPI_MINLOC,                       /* MPI operation in reduction   */ 
      world                             /* Communicator                 */
    );
    uDist = minDist[0];
    u = minDist[1];
    if (u / nLocal == rank)             /* if vtx belongs this proc     */
    {
      localVisited[uLocal] = 1;         /* mark as visited              */
    }

    for (vLocal = 0; vLocal < nLocal; vLocal++)
    {
      if (!localVisited[vLocal])
      {
        newDist = uDist + localMatrix[u * nLocal + vLocal];
        if (newDist < localDist[vLocal])
        {
          localDist[vLocal] = newDist;
        }
      }
    }

  }
}

/**
 * Prints distance from initial source to each vertex in the console
 * @input  dist: array of distances
 * @output none
 */
void printDists (int *dist)
{
  int i;
  printf ("   v    dist 0->v\n");
  printf ("----    ---------\n");
  for (i = 1; i < N_VTX; i++)
  {
    printf ("%4d       %5d\n", i, dist[i]);
  }
  printf ("----    ---------\n");
  free (dist);
}